package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> table;
    public Quoter() {
        table = new HashMap<>();
        table.put("1", 10.0);
        table.put("2", 45.0);
        table.put("3", 20.0);
        table.put("4", 35.0);
        table.put("5", 50.0);
    }
    public double getBookPrice(String isbn) {
        Double value = table.get(isbn);
        if (value == null) {
            return 0;
        }
        return value;
    }
}
